i3 is happy.

I just customised it a bit so i will be able to open & control unlimited  workspaces independently on a 3 monitors setup, without breaking my fingers on impossible key combinations and without using sequential key strokes. There are too many hot keys to remember as is.

#-------------------------------------------------------------------------------------------------------------------------------
The original code was posted on unix.StackExchange.com about six yeras ago (2017)
#-------------------------------------------------------------------------------------------------------------------------------
  
I've done this by storing the "toggle" variable to a file in `/tmp`. 

We can't use environment variables for the toggle, because each `bindsym exec` spawns a new shell and there's no way to set the environment variable in a parent process. 

In most distros, `/tmp` is `tmpfs` and stored in RAM, so it's still pretty fast to use.

Mod+= increases the "workspace modifier", adding 10 to every workspace number.

Mod+- decreases the "workspace modifier", subtracting 10 from every workspace number.

Normally, pressing Mod+1 will bring you to **workspace 1**. After pressing Mod+=, it will bring you to **workspace 11**.

Note that in this config, Mod+0 moves to **workspace 0** instead of **workspace 10**. If this is important to you, you may wish to use these bindings:

```
bindsym $mod+0 exec n=`cat /tmp/workspace_modifier` w=`[ $n ] && echo $n || echo 0` && i3-msg workspace number `echo "$w*10 + 10" | bc`

bindsym $mod+Shift+0 exec n=`cat /tmp/workspace_modifier` w=`[ $n ] && echo $n || echo 0` && i3-msg move container to workspace number `echo "$w*10 + 10" | bc`

```

Keeping workspace names becomes very complicated because of the way i3 config handles variables: it does direct replacement instead of injecting them in the environment of exec commands, so it's not possible to retrieve the values dynamically without redeclaring them in the exec command. This quickly gets messy:

```
set $ws1 "1: one"
set $ws2 "2: two"
set $ws1 "11: eleven"
set $ws2 "12: twelve"

bindsym $mod+Shift+1 exec key=1 ws1=$ws1 ws2=$ws2 ws11=$ws11 ws12=$ws12 && i=`cat /tmp/workspace_modifier` && dynamic=ws${i}${key} && text=${!dynamic} && workspace=$(echo `[ "$text" ] && echo "$text" || echo ${i}${key}` ) && i3-msg move container to workspace number $workspace
```

This should works as long as in the exec command you only declare the workspaces that have been named using the `set` command. 

If, for example, you define `set $ws1 "1: text"` but do not set `$ws11`, then include `ws11=$ws11` in the exec command, i3 config will expand `ws11=$ws11` to `ws11="1: one"1` because it matches `$ws1` in `$ws11`. It works fine otherwise.

If you wish to use workspace names plus keep Mod+0 set to **workspace 10**, use these bindsyms:

```
bindsym $mod+0 exec key=0 ws1=$ws1 ws2=$ws2 ws3=$ws3 ws4=$ws4 ws5=$ws5 ws6=$ws6 ws7=$ws7 ws8=$ws8 ws9=$ws9 ws10=$ws10 && n=`cat /tmp/workspace_modifier` w=`[ $n ] && echo $n || echo 0` i=`echo "$w*10 + 10" | bc` && dynamic=ws${i} && text=${!dynamic} && workspace=$(echo `[ "$text" ] && echo "$text" || echo ${i}` ) && i3-msg workspace number $workspace

bindsym $mod+Shift+0 exec key=0 ws0=$ws0 ws1=$ws1 ws2=$ws2 ws3=$ws3 ws4=$ws4 ws5=$ws5 ws6=$ws6 ws7=$ws7 ws8=$ws8 ws9=$ws9 ws10=$ws10 && n=`cat /tmp/workspace_modifier` w=`[ $n ] && echo $n || echo 0` i=`echo "$w*10 + 10" | bc` && dynamic=ws${i} && text=${!dynamic} && workspace=$(echo `[ "$text" ] && echo "$text" || echo ${i}` ) && i3-msg move container to workspace number "$workspace"
```
#-------------------------------------------------------------------------------------------------------------------------------
Source:
https://unix.stackexchange.com/questions/338228/i3wm-more-than-10-workspaces-with-double-modifier-key